package com.hcl.week2;

import java.util.ArrayList;
import java.util.Scanner;
public class Main {
	public static void main(String[] args) {
        ArrayList<Employee> employees = new ArrayList<>();
        	Employee emp1 = new Employee(1, "Aman", 20, 1100000, "IT", "Delhi");
        	Employee emp2 = new Employee(2, "Bobby", 22, 500000, "HR", "Bombay");
        	Employee emp3 = new Employee(3, "Zoe", 20, 750000, "Admin", "Delhi");
        	Employee emp4 = new Employee(4, "Smitha", 20, 1000000, "IT", "Chennai");
        	Employee emp5 = new Employee(5, "Smitha", 24, 1200000, "HR", "Bengaluru");
            employees.add(emp1);
            employees.add(emp2);
            employees.add(emp3);
            employees.add(emp4);
            employees.add(emp5);
            
            while(true) {
            	
            	Scanner sc = new Scanner(System.in);	
            	System.out.println("Employee Details:"+"Choose your option from below: "+"\n\n"
            						+"1. Display Details of employees."+"\n"
            						+"2.Names of all employees in the sorted order."
            						+"\n"+"3.Count of Employees from each city."+"\n"
            						+"4. Monthly salary of each Employee."+"\n"
            						+"5.End the process.");
            	System.out.print("\nEnter the option :");
            	
            	int select = sc.nextInt();
            	
            	switch(select) {
            		case 1: 
            			
            			// list of employees
            			
            			System.out.println("\n"+"List of Employees:"+"\n\n"+"-----------------------------------------------------------------"
    	            						+"\n"+"S.No"+"\t"+" NAME"+"\t"+"  AGE"+"\t"+"SALARY(INR)"+"\t"+"DEPARTMENT"+"\t"+"LOCATION");
            			
            			System.out.println("-----------------------------------------------------------------");
            			
            			for(int i=0;i<5;i++) { 
            				System.out.println(employees.get(i));
            				}
            			
            			System.out.println("-----------------------------------------------------------------");
    	            	System.out.println();
    	                break;
    	                
            		case 2:
            			
            			//display names in sorted order
            			DataStructureA dataA = new DataStructureA();
    	                
    	            	System.out.println();
    	            	dataA.sortingNames(employees);
    	            	System.out.println();
    	            	break;
    	            	
            		case 3:
            			// display count of employees from city
            			DataStructureB  dataB= new DataStructureB();
    	                 
    	        		 System.out.println();
    	        		 dataB.cityNameCount(employees);
    	                 System.out.println();
    	                 break;
    	                 
            		case 4:
    	        		// display monthly salary 
    	        		DataStructureB dataC = new DataStructureB();
    	        		
    	        		System.out.println();
    	        		dataC.monthlySalary(employees);
    	                System.out.println();
    	                break;
    	                
            		case 5:
            			System.exit(0);
            			
            		default:
    	 	        	System.err.println("Please Enter the valid option."+"\n");
    	            	
   	
    	        }
            	
            }
	}

}